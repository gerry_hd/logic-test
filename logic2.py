import sys

def get_last_facing_direction(n, m):
  if n > m:
    if m % 2 == 0:
      return "U"
    else:
      return "D"
  else:
    if n % 2 == 0:
      return "L"
    else:
      return "R"

def logic_test(lines):
  number_tests = lines[0]
  grids = lines[1:]
  for grid in grids:
    numbers_array = grid.split(' ')
    rows = int(numbers_array[0])
    columns = int(numbers_array[1])
    print get_last_facing_direction(rows, columns)


if __name__ == '__main__':
  f = open(sys.argv[1])
  lines = f.readlines()
  f.close()
  logic_test(lines)